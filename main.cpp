#include <iostream>
#include <vector>
#include <map>
#include <limits>
#include <chrono>
#include <thread>
#include "Room.h"
#include "Item.h"
#include "Character.h"
#include "Area.h"  
 // Include the Room and Item classes (assuming they are defined in separate
// files, which they should be!)
int main() {
  // Create an instance of the Area class
  Area gameWorld;

  // Load the game map from a text file
  gameWorld.LoadMapFromFile("game_map.txt");
  // Create a Player
  Player player("Bob", 100);
  // Set the player's starting room (you can modify this room name)
  Room* currentRoom = gameWorld.GetRoom("Starting Room");
  player.SetLocation(currentRoom);

  // Create Items
  Item key("Key", "A shiny key that looks important.");
  Item sword("Sword", "A sharp sword with a golden hilt.");
  Item skull("Skull", "The skull of a boss... long dead.");
  // Add items to rooms
  gameWorld.GetRoom("Starting Room")->AddItem(key);
  gameWorld.GetRoom("Treasure Room")->AddItem(sword);
  gameWorld.GetRoom("Boss Room")->AddItem(skull);

  // Create Enemies
  Enemy enemy("Goblin", 50);
  //Set the enemy's starting location
  Room *goblinCave = gameWorld.GetRoom("Goblin Cave");
  enemy.SetLocation(goblinCave);
  // Game loop (basic interaction)
  while (true) {
    std::cout << "Current Location: " << player.GetLocation()->GetDescription() << std::endl;
    std::cout << "Items in the room:" << std::endl;

    std::vector < Item > items = player.GetLocation() -> GetItems();
    for (Item item: items) {
      std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
    }
    std::cout << "Options: ";
    std::cout << "1. Look around | ";
    std::cout << "2. Interact with an item | ";
    std::cout << "3. Move to another room | ";
    std::cout << "4. Fight | ";  // New option for combat
    std::cout << "5. Quit" << std::endl;
    // Input handling
    int choice;
    if (!(std::cin >> choice) || std::cin.peek() != '\n') {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "Invalid input. Please enter an integer." << std::endl;
        continue; // Restart the loop
    }
    if (choice == 1) {
      // Player looks around 
      std::cout << "You look around the room." << std::endl;
    } else if (choice == 2) {
      // Player interacts with an item in the room
      std::cout << "Enter the name of the item you want to interact with: ";
      std::string itemName;
      std::cin >> itemName;
      for (Item item: player.GetLocation() -> GetItems()) {
        if (item.GetName() == itemName) {
          item.Interact();
          player.AddToInventory(item);
          player.GetLocation() -> RemoveItem(item);
          std::cout << "You picked up the " << item.GetName() << std::endl;
          break;
        }
      }
    } else if (choice == 3) {
      // Player moves to another room
      std::cout << "Enter the direction (e.g., north, south): ";
      std::string direction;
      std::cin >> direction;
      Room * nextRoom = player.GetLocation() -> GetExit(direction);
      if (nextRoom != nullptr) {
        player.SetLocation(nextRoom);
        std::cout << "You move to the next room." << std::endl;
      } else {
        std::cout << "You can't go that way." << std::endl;
      }
    } else if (choice == 4) {
      // Player enters combat with the enemy
            std::cout << "Entering combat... Get Ready!" << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(3)); 
            // Check if the player and enemy are in the same location
            if (player.GetLocation() == enemy.GetLocation()) {
                // Fight until one of them is defeated
                while (player.IsAlive() && enemy.IsAlive()) {
                    // Player attacks enemy
                    int playerDamage = player.Attack();
                    enemy.TakeDamage(playerDamage);
                    std::cout << "You hit the enemy for " << playerDamage << " damage." << std::endl;
                    std::this_thread::sleep_for(std::chrono::seconds(1)); 

                    // Enemy attacks player
                    int enemyDamage = enemy.Attack();
                    player.TakeDamage(enemyDamage);
                    std::cout << "The enemy hits you for " << enemyDamage << " damage." << std::endl;
                    std::this_thread::sleep_for(std::chrono::seconds(1)); 
                }

                // Check the outcome of the fight
                if (player.IsAlive()) {
                    std::cout << "!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
                    std::cout << "You defeated the Boss!" << std::endl;
                    std::cout << "!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
                } else {
                    std::cout << "You were defeated by the Boss!" << std::endl;
                    std::this_thread::sleep_for(std::chrono::seconds(3));
                    // Handle game over or other actions upon player's defeat
                    break; // End the game loop
                }
            } else {
                std::cout << "There's no one to fight here." << std::endl;
            }
    } else if (choice == 5) {
      // Quit the game
      std::cout << "Goodbye!" << std::endl;
      break;
    } else {
      std::cout << "Invalid choice. Try again." << std::endl;
    }
  }
  return 0;
}
