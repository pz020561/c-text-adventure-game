#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include "Room.h" // Assuming Room class is defined in Room.h

class Area {
private:
    std::map<std::string, Room*> rooms;

public:
    // Function to add a room to the area
    void AddRoom(const std::string& name, Room* room) {
        rooms[name] = room;
    }

    // Function to retrieve a room by its name
    Room* GetRoom(const std::string& name) {
        auto it = rooms.find(name);
        if (it != rooms.end()) {
            return it->second;
        } else {
            return nullptr;
        }
    }

    // Function to connect two rooms using a specified direction
    void ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction) {
        Room* room1 = GetRoom(room1Name);
        Room* room2 = GetRoom(room2Name);
        if (room1 && room2) {
            room1->AddExit(direction, room2);
        }
    }

    // Function to load the game map from a text file
    void LoadMapFromFile(const std::string& filename) {
        std::ifstream file(filename);
        if (file.is_open()) {
            std::string line;
            while (std::getline(file, line)) {
                std::istringstream iss(line);
                std::string room1Name, room2Name, direction;
                // read the room names and direction
                std::getline(iss, room1Name, '|');
                std::getline(iss, room2Name, '|');
                std::getline(iss, direction);

                Room *room1 = GetRoom(room1Name);
                if (room1 == nullptr) {
                    room1 = new Room(room1Name);
                    AddRoom(room1Name, room1);
                }
                Room *room2 = GetRoom(room2Name);
                if (room2 == nullptr) {
                    room2 = new Room(room2Name);
                    AddRoom(room2Name, room2);
                }
                ConnectRooms(room1Name, room2Name, direction);
            }
            file.close();
        } else {
            std::cerr << "Unable to open file: " << filename << std::endl;
        }
    }
};

