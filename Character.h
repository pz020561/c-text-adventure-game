#ifndef CHARACTER_H
#define CHARACTER_H

#include <iostream>
#include <string>
#include <vector>
#include "Item.h"
#include "Room.h"

class Character {
protected:
    std::string name;
    int health;
    std::vector<Item> inventory;
public:
    Character(const std::string& name, int health) : name(name), health(health) {}

    void TakeDamage(int damage) {
        health -= damage;
    }

    void AddToInventory(const Item& item) {
        inventory.push_back(item);
    }
};

class Player : public Character {
private:
    Room* location;
public:
    Player(const std::string& name, int health) : Character(name, health), location(nullptr) {}

    void SetLocation(Room* location) {
        this->location = location;
    }

    Room* GetLocation() const {
        return location;
    }

    bool IsAlive() const {
        return health > 0;
    }

    int Attack() const {
        return 20; // Default attack value for the player
    }
};

class Enemy : public Character {
private:
    Room* location;
public:
    Enemy(const std::string& name, int health) : Character(name, health), location(nullptr) {}

    int Attack() const {
        return 10; // Default attack value for the enemy
    }

    void TakeDamage(int damage) {
        health -= damage;
    }

    bool IsAlive() const {
        return health > 0;
    }

    void SetLocation(Room* location) {
        this->location = location;
    }

    Room* GetLocation() const {
        return location;
    }
};

#endif
