#ifndef ROOM_H
#define ROOM_H

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "Item.h"

class Room {
private:
    std::string description;
    std::map<std::string, Room*> exits;
    std::vector<Item> items;
     
public:
    Room(const std::string& desc) : description(desc) {}

    void AddItem(const Item& item) {
        items.push_back(item);
    }

	 void RemoveItem(const Item& itemToRemove) {
        for (auto it = items.begin(); it != items.end(); ++it) {
            if (*it == itemToRemove) {
                items.erase(it);
                break;
            }
        }
    }

    std::vector<Item> GetItems() const {
        return items;
    }
	
Room* GetExit(const std::string& direction) const {
    auto it = exits.find(direction);
    if (it != exits.end()) {
        return it->second;
    } else {
        return nullptr;
    }
}

    std::string GetDescription() const {
        return description;
    }

    void AddExit(const std::string& dir, Room* room) {
        exits.insert(std::make_pair(dir, room));
    }
};

#endif
