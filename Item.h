#ifndef ITEM_H
#define ITEM_H

#include <iostream>
#include <string>

class Item {
private:
    std::string name;
    std::string description;
public:
    Item(const std::string& name, const std::string& desc) {
        this->name = name;
        description = desc;
    }

    bool operator==(const Item& other) const {
        return name == other.name && description == other.description;
    }

    void Interact() {
        std::cout << description << std::endl;
    }

    std::string GetDescription() const {
        return description;
    }
    
    std::string GetName() const {
        return name;
    }
};

#endif